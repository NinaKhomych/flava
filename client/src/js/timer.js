function getTimeRemaining(endtime) {
  const t = Date.parse(endtime) - Date.parse(new Date());
  const seconds = Math.floor((t / 1000) % 60);
  const minutes = Math.floor((t / 1000 / 60) % 60);
  const hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  const days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}
 
function initializeClock(id, endtime) {
  const clock = document.getElementById(id);
  const [daysSpan] = clock.getElementsByClassName('js-days');
  const [hoursSpan] = clock.getElementsByClassName('js-hours');
  const [minutesSpan] = clock.getElementsByClassName('js-minutes');
  const [secondsSpan] = clock.getElementsByClassName('js-seconds');
 
  function updateClock() {
    const t = getTimeRemaining(endtime);
 
    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
 
    if (t.total <= 0) {
      clearInterval(timeinterval);
    }
  }
 
  updateClock();
  const timeinterval = setInterval(updateClock, 1000);
}
 
let deadline = "March 1 2022 00:00:00 GMT+0200";
initializeClock('countdown', deadline);
