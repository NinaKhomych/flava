const [burgerMenu] = document.getElementsByClassName('js-nav');
const [burger] = document.getElementsByClassName('js-burger');
const [button] = document.getElementsByClassName('js-button');

const burgerToggle = function() {
  burger.classList.toggle('is-active');
  burgerMenu.classList.toggle('is-active');
  button.classList.toggle('is-active');
};

burger.addEventListener('click', burgerToggle);

document.addEventListener('click', function(e) {
  const target = e.target;
  const menu = target == burgerMenu || burgerMenu.contains(target);
  const burgerBtn = target == burger || burger.contains(target);
  const activeMenu = burgerMenu.classList.contains('is-active');
  if (!menu && activeMenu && !burgerBtn) {
    burgerToggle();
  }
});