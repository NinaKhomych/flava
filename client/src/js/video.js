const [play] = document.getElementsByClassName('js-btn-play');
const [video] = document.getElementsByClassName('js-video');
const [picture] = document.getElementsByClassName('js-picture');



const videoOn = function() {
  play.classList.toggle('is-active');
  video.classList.toggle('is-active');
  picture.classList.toggle('is-active');
}

play.addEventListener('click', videoOn);
