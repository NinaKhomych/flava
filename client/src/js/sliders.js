import slick from 'slick-carousel';
import $ from 'jquery';


const slickOptions = {
  arrows: true,
  appendArrows: $('.lineup__arrows'),
  nextArrow: '<a class="control"><img class="control__img" src="../static/img/next.png"/> </a>',
  prevArrow: '<a class="control"><img class="control__img" src="../static/img/prev.png"/> </a>',
  dots: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  mobileFirst: true,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 3
      }
    },
  ],
}
$('.js-slider-lineup').slick(slickOptions)

const slickOptions2 = {
  arrows: false,
  dots: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  mobileFirst: true,
  responsive: [
    {
      breakpoint: 575,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 5,
      }
    },
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 6,
        slidesToScroll: 3,
      }
    },
  ],
}
$('.js-slider-sponsors').slick(slickOptions2)