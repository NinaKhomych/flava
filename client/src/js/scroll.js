const scrollsUp = Array.from(document.getElementsByClassName('js-scroll'));

scrollsUp.forEach(scrollUp => {
  window.addEventListener('scroll', function() {
     if (window.pageYOffset > 150) {
        scrollUp.classList.add('shown');
     } else {
        scrollUp.classList.remove('shown');
     }
  });

  scrollUp.addEventListener('click', function() {
     window.scrollTo( {
       top: 0, behavior: "smooth"
     });
  });
});